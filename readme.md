# HJI Page Builder Templates

## Importing into WordPress

1. Compress your XML files into GZ format, for example: `tar -cvzf latitude.xml.gz latitude.xml`
2. Import the compressed GZ files.

Importing large XML files without compression may result in failure with 413 or 499 error codes.

## Change Log

v.0.3
- Moved Hero blocks into theme-specific files (arroyo.xml, central.xml, latitude.xml)
- Moved the rest of the blocks into common-blocks.xml


v.0.2
- Adds Home Page 1 template to Latitude. 
